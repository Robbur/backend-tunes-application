package com.noroff.task5tunes.data_access;

import com.noroff.task5tunes.models.music_models.Artist;
import com.noroff.task5tunes.models.music_models.Genre;
import com.noroff.task5tunes.models.music_models.MusicSearch;
import com.noroff.task5tunes.models.music_models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class HomePageRepository {

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    //Returns 5 random artists
    public ArrayList<Artist> getRandomArtists() {
        ArrayList<Artist> artists = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT Name " +
                            "FROM artist " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                artists.add(new Artist(
                        set.getString("Name")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return artists;
    }

    //Returns 5 random songs
    public ArrayList<Track> getRandomSongs() {
        ArrayList<Track> songs = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT Name " +
                            "FROM track " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                songs.add(new Track(
                        set.getString("Name")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return songs;
    }


    //Returns 5 random genres
    public ArrayList<Genre> getRandomGenres() {
        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT Name " +
                            "FROM genre " +
                            "ORDER BY RANDOM() " +
                            "LIMIT 5");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                genres.add(new Genre(
                        set.getString("Name")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return genres;
    }

    //Returns all songs equal to the search query
    public ArrayList<MusicSearch> getSearchResult(String searchQuery) {
        ArrayList<MusicSearch> musicSearchList = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT track.Name as TrackName, artist.Name as ArtistName, album.Title as AlbumTitle, genre.Name as GenreName " +
                            "FROM track " +
                            "NATURAL JOIN album " +
                            "INNER JOIN artist on artist.ArtistId = album.ArtistId " +
                            "INNER JOIN genre on genre.GenreId = track.GenreId " +
                            "WHERE track.Name LIKE '%" + searchQuery + "%'");
            ResultSet set = prep.executeQuery();

            while (set.next()) {
                musicSearchList.add(new MusicSearch(
                        set.getString("TrackName"),
                        set.getString("ArtistName"),
                        set.getString("AlbumTitle"),
                        set.getString("GenreName")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return musicSearchList;
    }
}

package com.noroff.task5tunes.data_access;

import com.noroff.task5tunes.models.customer_models.Customer;
import com.noroff.task5tunes.models.customer_models.CustomerCountryCount;
import com.noroff.task5tunes.models.customer_models.CustomerHighestSpender;
import com.noroff.task5tunes.models.customer_models.CustomerPopularGenre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CustomerRepository {

    private final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    // Read all the customers in the database, this will display their:
    // Id, first name, last name, country, postal code, and phone number.
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone " +
                            "FROM customer");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                customers.add(new Customer(
                        set.getString("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    // Add a new customer to the database, with a hard coded email and employee support rep
    public Boolean addCustomer(Customer customer) {

        boolean success = false;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "INSERT INTO customer(FirstName, LastName, Country, PostalCode, Phone, Email, SupportRepId) " +
                            "VALUES(?,?,?,?,?,?,?)");
            prep.setString(1, customer.getFirstName());
            prep.setString(2, customer.getLastName());
            prep.setString(3, customer.getCountry());
            prep.setString(4, customer.getPostalCode());
            prep.setString(5, customer.getPhone());
            prep.setString(6, "customerEmail@gmail.com");
            prep.setString(7, "1");

            int result = prep.executeUpdate();
            success = (result != 0);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    // Update an existing customer.
    public Boolean updateCustomer(Customer customer) {
        boolean success = false;

        try {
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep = conn.prepareStatement(
                    "UPDATE customer SET FirstName=?, LastName=?, " +
                            "Country=?, PostalCode=?, Phone=?, Email=?, SupportRepId=? " +
                            "WHERE CustomerId=?");
            prep.setString(1, customer.getFirstName());
            prep.setString(2, customer.getLastName());
            prep.setString(3, customer.getCountry());
            prep.setString(4, customer.getPostalCode());
            prep.setString(5, customer.getPhone());
            prep.setString(6, "customerEmail@gmail.com");
            prep.setString(7, "1");
            prep.setString(8, customer.getCustomerId());

            int result = prep.executeUpdate();
            success = (result != 0);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    // Return the number of customers in each country, ordered descending
    // Most number of customers to least
    public ArrayList<CustomerCountryCount> getCountryCustomers() {
        ArrayList<CustomerCountryCount> customerCountryCount = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT Country, COUNT(*) " +
                            "FROM customer " +
                            "GROUP BY Country " +
                            "ORDER BY COUNT(*) DESC");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                customerCountryCount.add(new CustomerCountryCount(
                        set.getString("Country"),
                        set.getInt("COUNT(*)")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return customerCountryCount;
    }

    // Returns customers who are the  highest spenders ( total in invoice table as the largest), ordered descending.
    public ArrayList<CustomerHighestSpender> getHighestSpenders() {
        ArrayList<CustomerHighestSpender> customerHighestSpender = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT  customer.CustomerId, FirstName, LastName, Country, SUM(invoice.Total) as AmountSpent " +
                            "FROM customer " +
                            "INNER JOIN invoice " +
                            "ON customer.CustomerId = invoice.CustomerId " +
                            "GROUP BY customer.CustomerId " +
                            "ORDER BY AmountSpent DESC");

            ResultSet set = prep.executeQuery();

            while (set.next()) {
                customerHighestSpender.add(new CustomerHighestSpender(
                        set.getString("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getDouble("AmountSpent")
                ));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }

        return customerHighestSpender;
    }

    // For a given customer, returns their most popular genre (in case of a tie, display all).
    // Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.
    public ArrayList<CustomerPopularGenre> getCustomerPopularGenre(String id) {
        ArrayList<CustomerPopularGenre> customerPopularGenres = new ArrayList<>();
        ArrayList<CustomerPopularGenre> modifiedCustomerPopularGenres = new ArrayList<>();
        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "SELECT FirstName, LastName, Country, genre.Name as GenreName, COUNT(genre.Name) as GenreCount" +
                            " FROM customer" +
                            " INNER JOIN invoice on customer.CustomerId = invoice.CustomerId" +
                            " INNER JOIN invoiceline on invoice.InvoiceId = invoiceline.InvoiceId" +
                            " INNER JOIN track on invoiceline.TrackId = track.TrackId" +
                            " INNER JOIN genre on track.GenreId = genre.GenreId" +
                            " WHERE customer.CustomerId =?" +
                            " GROUP BY genre.GenreId" +
                            " ORDER BY COUNT(genre.Name) DESC");

            prep.setString(1, id);
            ResultSet set = prep.executeQuery();

            while (set.next()) {
                customerPopularGenres.add(new CustomerPopularGenre(
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("GenreName"),
                        set.getInt("GenreCount")
                ));
            }

            int maxCount = customerPopularGenres.get(0).getGenreCount();
            // Find the highest count for a genre
            for (CustomerPopularGenre popularGenre : customerPopularGenres) {
                if (popularGenre.getGenreCount() > maxCount) {
                    maxCount = popularGenre.getGenreCount();
                }
            }
            // Add all genre counts equal to the highest count
            for (CustomerPopularGenre customerPopularGenre : customerPopularGenres) {
                if (customerPopularGenre.getGenreCount() == maxCount) {
                    modifiedCustomerPopularGenres.add(customerPopularGenre);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                conn.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        // Returns the modified list with all genres equal to the highest count
        return modifiedCustomerPopularGenres;
    }
}

package com.noroff.task5tunes.models.customer_models;

public class CustomerPopularGenre {

    private String firstName;
    private String lastName;
    private String country;
    private String genreName;
    private int genreCount;

    public CustomerPopularGenre() {
    }

    public CustomerPopularGenre(String firstName, String lastName, String country, String genreName, int genreCount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.genreName = genreName;
        this.genreCount = genreCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getGenreCount() {
        return genreCount;
    }

    public void setGenreCount(int genreCount) {
        this.genreCount = genreCount;
    }
}

package com.noroff.task5tunes.models.customer_models;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CustomerHighestSpender {

    private String customerId;
    private String firstName;
    private String lastName;
    private String country;
    private double amountSpent;

    public CustomerHighestSpender() {
    }

    public CustomerHighestSpender(String customerId, String firstName, String lastName, String country, double amountSpent) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;

        BigDecimal bd = BigDecimal.valueOf(amountSpent);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        this.amountSpent = bd.doubleValue();
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getAmountSpent() {
        return amountSpent;
    }

    public void setAmountSpent(double amountSpent) {
        this.amountSpent = amountSpent;
    }
}

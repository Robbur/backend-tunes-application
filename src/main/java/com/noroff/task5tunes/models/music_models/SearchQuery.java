package com.noroff.task5tunes.models.music_models;

public class SearchQuery {
    private String term;

    public SearchQuery() {
    }

    public SearchQuery(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }
}

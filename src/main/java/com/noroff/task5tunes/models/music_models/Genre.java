package com.noroff.task5tunes.models.music_models;

public class Genre {
    private String genreId;
    private String name;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public Genre(String genreId, String name) {
        this.genreId = genreId;
        this.name = name;
    }

    // Getters and Setters
    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

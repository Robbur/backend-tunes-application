package com.noroff.task5tunes.controllers;

import com.noroff.task5tunes.data_access.HomePageRepository;
import com.noroff.task5tunes.models.music_models.SearchQuery;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class HomePageController {
    HomePageRepository homePageRepository = new HomePageRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("artists", homePageRepository.getRandomArtists());
        model.addAttribute("songs", homePageRepository.getRandomSongs());
        model.addAttribute("genres", homePageRepository.getRandomGenres());
        model.addAttribute("search", new SearchQuery());
        return "index";
    }

    // When search button is clicked, and the input field is not empty, navigate to search result page and show result
    @GetMapping("/search")
    public String searchResult(@Valid SearchQuery searchQuery, Model model) {
        model.addAttribute("search", searchQuery);
        model.addAttribute("songs", homePageRepository.getSearchResult(searchQuery.getTerm()));
        return "search-result";
    }
}


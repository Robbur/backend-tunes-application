package com.noroff.task5tunes.controllers;

import com.noroff.task5tunes.data_access.CustomerRepository;
import com.noroff.task5tunes.models.customer_models.Customer;
import com.noroff.task5tunes.models.customer_models.CustomerCountryCount;
import com.noroff.task5tunes.models.customer_models.CustomerHighestSpender;
import com.noroff.task5tunes.models.customer_models.CustomerPopularGenre;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    CustomerRepository customerRepository = new CustomerRepository();

    // Read all the customers in the database, this will display their:
    // Id, first name, last name, country, postal code, and phone number.
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    // Add a new customer to the database, with a hard coded email and employee support rep
    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public Boolean addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    // Update an existing customer.
    @RequestMapping(value = "/api/customers", method = RequestMethod.PUT)
    public Boolean updateCustomer(@RequestBody Customer customer) {
        return customerRepository.updateCustomer(customer);
    }

    // Return the number of customers in each country, ordered descending
    // Most number of customers to least
    @RequestMapping(value = "/api/customers/top/country", method = RequestMethod.GET)
    public ArrayList<CustomerCountryCount> getCountryCustomers() {
        return customerRepository.getCountryCustomers();
    }

    // Returns customers who are the  highest spenders ( total in invoice table as the largest), ordered descending.
    @RequestMapping(value = "/api/customers/top/spender", method = RequestMethod.GET)
    public ArrayList<CustomerHighestSpender> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

    // For a given customer, returns their most popular genre (in case of a tie, display all).
    // Most popular in this context means the genre that corresponds to the most tracks from invoices associated to that customer.
    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public ArrayList<CustomerPopularGenre> getCustomerPopularGenre(@PathVariable String id) {
        return customerRepository.getCustomerPopularGenre(id);
    }
}

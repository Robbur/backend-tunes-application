// Variable for search button, setting to hidden before text is added to input field
let inputButton = document.getElementById("inputButton");
inputButton.style.visibility = "hidden"

// Variable for input form and input field
let inputForm = document.getElementById("inputForm");
let inputField = document.getElementById("inputField");

// Hides the button when the input field is empty
inputField.oninput = (event) => {
    if (event.target.value == ""){
        inputButton.style.visibility = "hidden"
    }else{
        inputButton.style.visibility = "visible"
    }
}

// Stops user from submitting empty search query by pressing enter
inputForm.addEventListener("keydown", function(event) {
    if (event.key == "Enter" && inputField.value == "") {
        event.preventDefault();
    }
})

# Task 5 - Tunes

Project was built using Thymeleaf and the Chinook_Sqlite.sqlite database.

The purpose of this application was to create API endpoints that can be used to gather data from the Chinook database.
There is also a home page, showing 5 random songs, artists and genres.
From here, you can also search for a song in the Chinook database, which will take you to a new page where the result of 
your search is displayed.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Prerequisites

What things you need to install the software and how to install them

Install the latest version of Java JDK: 
[www.oracle.com](https://www.oracle.com/java/technologies/javase-jdk14-downloads.html)

### Running the project

Open the project in an IDE of your choice and run the HomePageApplication.java class.
This file is located under task-5-tunes/src/main/java/com.noroff.task5tunes/HomePageApplication.java.

The application can also be accessed on Heroku here: 
[Link to Heroku app](https://task-5-tunes-docker.herokuapp.com/)

In the resources folder, you will find a Postman collection, allowing you to use all the API endpoints.
The API endpoints are the following:

1. Retrieve all the customers in the database (GET): <br/>
https://task-5-tunes.herokuapp.com/api/customers

2. Add a new customer to the database (POST). <br/>
https://task-5-tunes.herokuapp.com/api/customers

3. Update an existing customer (PUT). <br/>
https://task-5-tunes.herokuapp.com/api/customers

4. Return the number of customers in each country, ordered descending (GET). <br/>
https://task-5-tunes.herokuapp.com/api/customers/top/country

5. Retrieve the customers who are the highest spenders, ordered descending (GET). <br/>
https://task-5-tunes.herokuapp.com/api/customers/top/spender

6. For a given customer, show their most popular genre(s) (GET). <br/>
https://task-5-tunes.herokuapp.com/api/customers/{id}/popular/genre

## Maintainers 

[Robin Burø (@Robbur)](https://gitlab.com/Robbur)

## License

---
Copyright 2020, Robin Burø ([@Robbur](https://gitlab.com/Robbur))
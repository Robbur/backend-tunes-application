FROM openjdk:11
ADD target/task-5-tunes-0.0.1-SNAPSHOT.jar task-5-tunes.jar
ENTRYPOINT [ "java", "-jar", "/task-5-tunes.jar" ]